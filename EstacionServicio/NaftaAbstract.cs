﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstacionServicio
{
    public abstract class NaftaAbstract
    {
        private float precioLitro;
        private string nombre;

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }


        public float PrecioLitro { get => precioLitro; set => precioLitro = value; }

        public float getPrecio()
        {
            return PrecioLitro;
        }


    }
}
