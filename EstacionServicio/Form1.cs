﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EstacionServicio
{
    public partial class Form1 : Form
    {
        EstacionServicio estacionServicio = new EstacionServicio();  

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cargarSurtidores();            
        }

        private void cargarSurtidores()
        {
            cmbSurtidores.Items.Clear();
            foreach(Surtidor surtidor in estacionServicio.Surtidores)
            {
                cmbSurtidores.Items.Add(surtidor);
                cmbSurtidores.DisplayMember = "NombreSurtidor";
            }
            cmbSurtidores.SelectedIndex = 0;
        }

        private void cmbSurtidores_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargarDatosSurtidor();
        }

        private void cargarDatosSurtidor()
        {
            lblCapacidadSurtidor.Text = estacionServicio.Surtidores[cmbSurtidores.SelectedIndex].Capacidad.ToString();
            lblPrecioNafta.Text = estacionServicio.Surtidores[cmbSurtidores.SelectedIndex].TipoNafta.PrecioLitro.ToString();
            lblTipoNafta.Text = estacionServicio.Surtidores[cmbSurtidores.SelectedIndex].TipoNafta.GetType().Name;
        }

        private void cmbSurtidores_Move(object sender, EventArgs e)
        {

        }

        private void nudLitros_ValueChanged(object sender, EventArgs e)
        {
            lblPrecioTotal.Text = estacionServicio.calcularPrecioTotal((Surtidor)cmbSurtidores.SelectedItem, int.Parse(nudLitros.Value.ToString())).ToString();
        }

        private void btnComprar_Click(object sender, EventArgs e)
        {
            if (nudLitros.Value != 0)
            {
                comprarNafta();
            }
            else
            {
                MessageBox.Show("DEBE INGRESAR UN LITRAJE MAYOR A CERO");
            }
        }
      

        private void comprarNafta()
        {
            estacionServicio.comprarNafta((Surtidor)cmbSurtidores.SelectedItem, int.Parse(nudLitros.Value.ToString()), float.Parse(lblPrecioTotal.Text));
            lblRecaudacionTotal.Text = estacionServicio.obtenerRecaudacionTotal().ToString();
            lblCapacidadSurtidor.Text = estacionServicio.Surtidores[cmbSurtidores.SelectedIndex].Capacidad.ToString();

            foreach (Surtidor surtidor in estacionServicio.Surtidores)
            {
                if (surtidor.TipoNafta.Nombre.Equals("NaftaNormal"))
                {
                    resusurt1.Text = estacionServicio.MostrarRecaudacionSurti(surtidor).ToString();
                }
                else if (surtidor.TipoNafta.Nombre.Equals("NaftaSuper"))
                {
                    resusurt2.Text = estacionServicio.MostrarRecaudacionSurti(surtidor).ToString();
                }
                else if (surtidor.TipoNafta.Nombre.Equals("NaftaPremium"))
                {
                    resusurt3.Text = estacionServicio.MostrarRecaudacionSurti(surtidor).ToString();
                }
            }
            cargarSurtidores();
            cleanControls();
        }

        private void cleanControls()
        {
            nudLitros.Value = nudLitros.Minimum;
            lblPrecioTotal.Text = "";
        }

        private void btnRecarga_Click(object sender, EventArgs e)
        {
            if (estacionServicio.Surtidores[cmbSurtidores.SelectedIndex].Capacidad < 100)
            {
                estacionServicio.Recargarsurtidor((Surtidor)cmbSurtidores.SelectedItem);
                lblCapacidadSurtidor.Text = estacionServicio.Surtidores[cmbSurtidores.SelectedIndex].Capacidad.ToString();
            }
            else
            {
                MessageBox.Show("EL SURTIDOR ESTA LLENO");
            }
        }

        private void btnMaxRec_Click(object sender, EventArgs e)
        {
            MessageBox.Show("SURTIDO DE MAYOR RECAUDACION: " + estacionServicio.getSurtidorMayorRecaudacion());
        }

        private void btnMinRec_Click(object sender, EventArgs e)
        {
            MessageBox.Show("SURTIDO DE MENOR RECAUDACION: " + estacionServicio.getSurtidorMenorRecaudacion());
        }

        private void btnMaxClient_Click(object sender, EventArgs e)
        {
            MessageBox.Show("SURTIDO CON MAYOR CANT DE CLIENTES: " + estacionServicio.getSurtidorMasClientes());
        }
    }
}