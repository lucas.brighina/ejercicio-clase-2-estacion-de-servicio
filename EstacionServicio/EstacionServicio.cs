﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstacionServicio
{
    public class EstacionServicio
    {
        public Surtidor[] Surtidores = new Surtidor[3];

        public EstacionServicio()
        {
            Surtidores[0] = new Surtidor("SurtidorNormal", 100);
            Surtidores[0].TipoNafta = new NaftaNormal("NaftaNormal", 17.20f);
            Surtidores[1] = new Surtidor("SurtidorSuper", 100);
            Surtidores[1].TipoNafta = new NaftaSuper("NaftaSuper", 18.85f);
            Surtidores[2] = new Surtidor("SurtidoPremium", 100);
            Surtidores[2].TipoNafta = new NaftaPremium("NaftaPremium", 21.30f);
        }

        public float calcularPrecioTotal(Surtidor surtidor, int litrosAComprar)
        {
            return litrosAComprar * surtidor.TipoNafta.PrecioLitro;
        }

        public void comprarNafta(Surtidor surtidor, int litrosAComprar, float precioFinal)
        {
            surtidor.TotalRecaudado += precioFinal;
            surtidor.CantidadClientes++;
            surtidor.RestarCapacidad(litrosAComprar);
        }

        //public Surtidor getSurtidor(string tipoSurtidor)
        //{
        //    foreach (Surtidor surt in Surtidores)
        //    {
        //        if (surt.TipoNafta.Nombre.Equals(tipoSurtidor) && surt != null) return surt;
        //    }
        //    return null;
        //}

        public float MostrarRecaudacionSurti(Surtidor surtidor)
        {
            return surtidor.TotalRecaudado;
        }

        public void Recargarsurtidor(Surtidor surtidor)
        {
            surtidor.Capacidad = 100;
            surtidor.CantidadRecargas++;
        }

        public float obtenerRecaudacionTotal()
        {
            return Surtidores[0].TotalRecaudado + Surtidores[1].TotalRecaudado + Surtidores[2].TotalRecaudado;
        }

        public float calcularRecaudacionSurtidor(Surtidor surtidor, int litrosAComprar)
        {
            surtidor.TotalRecaudado += litrosAComprar * surtidor.TipoNafta.PrecioLitro;
            return surtidor.TotalRecaudado;
        }

        public string getSurtidorMayorRecaudacion()
        {
            float mayorRecaudacion = -1f;
            string nombreSurt = "No hubo ventas";
            foreach (Surtidor surt in Surtidores)
            {
                if (surt.TotalRecaudado != 0)
                {
                    if (surt.TotalRecaudado > mayorRecaudacion || mayorRecaudacion == -1)
                    {
                        mayorRecaudacion = surt.TotalRecaudado;
                        nombreSurt = surt.TipoNafta.Nombre;
                    }
                }                                
            }
            return nombreSurt;
        }

        public string getSurtidorMenorRecaudacion()
        {
            float mayorRecaudacion = -1f;
            string nombreSurt = "No hubo ventas";
            foreach (Surtidor surt in Surtidores)
            {
                if (surt.TotalRecaudado != 0)
                {
                    if (surt.TotalRecaudado < mayorRecaudacion || mayorRecaudacion == -1)
                    {
                        mayorRecaudacion = surt.TotalRecaudado;
                        nombreSurt = surt.TipoNafta.Nombre;
                    }
                }
            }
            return nombreSurt;
        }

        public string getSurtidorMasClientes()
        {
            float mayorclientes = -1f;
            string nombreSurt = "No hubo ventas";
            foreach (Surtidor surt in Surtidores)
            {
                if (surt.CantidadClientes != 0)
                {
                    if (surt.CantidadClientes > mayorclientes || mayorclientes == -1)
                    {
                        mayorclientes = surt.CantidadClientes;
                        nombreSurt = surt.TipoNafta.Nombre;
                    }
                }
            }
            return nombreSurt;
        }

    }
}
