﻿namespace EstacionServicio
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbSurtidores = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblTipoNafta = new System.Windows.Forms.Label();
            this.sadsad = new System.Windows.Forms.Label();
            this.lblPrecioNafta = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblCapacidadSurtidor = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.nudLitros = new System.Windows.Forms.NumericUpDown();
            this.btnComprar = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.lblPrecioTotal = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnMaxClient = new System.Windows.Forms.Button();
            this.btnMinRec = new System.Windows.Forms.Button();
            this.btnMaxRec = new System.Windows.Forms.Button();
            this.lblRecaudacionTotal = new System.Windows.Forms.Label();
            this.total = new System.Windows.Forms.Label();
            this.resusurt3 = new System.Windows.Forms.Label();
            this.resusurt2 = new System.Windows.Forms.Label();
            this.resusurt1 = new System.Windows.Forms.Label();
            this.lblsurt3 = new System.Windows.Forms.Label();
            this.lblsurt2 = new System.Windows.Forms.Label();
            this.lblsurt1 = new System.Windows.Forms.Label();
            this.btnRecarga = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nudLitros)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbSurtidores
            // 
            this.cmbSurtidores.FormattingEnabled = true;
            this.cmbSurtidores.Location = new System.Drawing.Point(27, 73);
            this.cmbSurtidores.Name = "cmbSurtidores";
            this.cmbSurtidores.Size = new System.Drawing.Size(132, 21);
            this.cmbSurtidores.TabIndex = 0;
            this.cmbSurtidores.SelectedIndexChanged += new System.EventHandler(this.cmbSurtidores_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Surtidores";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(146, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Estacion de servicio";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Tipo Nafta:";
            // 
            // lblTipoNafta
            // 
            this.lblTipoNafta.AutoSize = true;
            this.lblTipoNafta.Location = new System.Drawing.Point(117, 129);
            this.lblTipoNafta.Name = "lblTipoNafta";
            this.lblTipoNafta.Size = new System.Drawing.Size(10, 13);
            this.lblTipoNafta.TabIndex = 4;
            this.lblTipoNafta.Text = "-";
            // 
            // sadsad
            // 
            this.sadsad.AutoSize = true;
            this.sadsad.Location = new System.Drawing.Point(27, 166);
            this.sadsad.Name = "sadsad";
            this.sadsad.Size = new System.Drawing.Size(37, 13);
            this.sadsad.TabIndex = 5;
            this.sadsad.Text = "Precio";
            // 
            // lblPrecioNafta
            // 
            this.lblPrecioNafta.AutoSize = true;
            this.lblPrecioNafta.Location = new System.Drawing.Point(117, 166);
            this.lblPrecioNafta.Name = "lblPrecioNafta";
            this.lblPrecioNafta.Size = new System.Drawing.Size(10, 13);
            this.lblPrecioNafta.TabIndex = 6;
            this.lblPrecioNafta.Text = "-";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 205);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Capacidad (L)";
            // 
            // lblCapacidadSurtidor
            // 
            this.lblCapacidadSurtidor.AutoSize = true;
            this.lblCapacidadSurtidor.Location = new System.Drawing.Point(117, 205);
            this.lblCapacidadSurtidor.Name = "lblCapacidadSurtidor";
            this.lblCapacidadSurtidor.Size = new System.Drawing.Size(10, 13);
            this.lblCapacidadSurtidor.TabIndex = 8;
            this.lblCapacidadSurtidor.Text = "-";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(245, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Litros a comprar";
            // 
            // nudLitros
            // 
            this.nudLitros.Location = new System.Drawing.Point(248, 73);
            this.nudLitros.Name = "nudLitros";
            this.nudLitros.Size = new System.Drawing.Size(99, 20);
            this.nudLitros.TabIndex = 10;
            this.nudLitros.ValueChanged += new System.EventHandler(this.nudLitros_ValueChanged);
            // 
            // btnComprar
            // 
            this.btnComprar.Location = new System.Drawing.Point(252, 166);
            this.btnComprar.Name = "btnComprar";
            this.btnComprar.Size = new System.Drawing.Size(75, 30);
            this.btnComprar.TabIndex = 11;
            this.btnComprar.Text = "Comprar";
            this.btnComprar.UseVisualStyleBackColor = true;
            this.btnComprar.Click += new System.EventHandler(this.btnComprar_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(245, 129);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Precio total";
            // 
            // lblPrecioTotal
            // 
            this.lblPrecioTotal.AutoSize = true;
            this.lblPrecioTotal.Location = new System.Drawing.Point(329, 129);
            this.lblPrecioTotal.Name = "lblPrecioTotal";
            this.lblPrecioTotal.Size = new System.Drawing.Size(10, 13);
            this.lblPrecioTotal.TabIndex = 13;
            this.lblPrecioTotal.Text = "-";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnMaxClient);
            this.groupBox1.Controls.Add(this.btnMinRec);
            this.groupBox1.Controls.Add(this.btnMaxRec);
            this.groupBox1.Controls.Add(this.lblRecaudacionTotal);
            this.groupBox1.Controls.Add(this.total);
            this.groupBox1.Controls.Add(this.resusurt3);
            this.groupBox1.Controls.Add(this.resusurt2);
            this.groupBox1.Controls.Add(this.resusurt1);
            this.groupBox1.Controls.Add(this.lblsurt3);
            this.groupBox1.Controls.Add(this.lblsurt2);
            this.groupBox1.Controls.Add(this.lblsurt1);
            this.groupBox1.Location = new System.Drawing.Point(397, 29);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(287, 360);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Resultados";
            // 
            // btnMaxClient
            // 
            this.btnMaxClient.Location = new System.Drawing.Point(22, 202);
            this.btnMaxClient.Name = "btnMaxClient";
            this.btnMaxClient.Size = new System.Drawing.Size(241, 30);
            this.btnMaxClient.TabIndex = 10;
            this.btnMaxClient.Text = "Surtidor con mas clientes";
            this.btnMaxClient.UseVisualStyleBackColor = true;
            this.btnMaxClient.Click += new System.EventHandler(this.btnMaxClient_Click);
            // 
            // btnMinRec
            // 
            this.btnMinRec.Location = new System.Drawing.Point(155, 160);
            this.btnMinRec.Name = "btnMinRec";
            this.btnMinRec.Size = new System.Drawing.Size(109, 28);
            this.btnMinRec.TabIndex = 9;
            this.btnMinRec.Text = "Menos Recaudador";
            this.btnMinRec.UseVisualStyleBackColor = true;
            this.btnMinRec.Click += new System.EventHandler(this.btnMinRec_Click);
            // 
            // btnMaxRec
            // 
            this.btnMaxRec.Location = new System.Drawing.Point(18, 161);
            this.btnMaxRec.Name = "btnMaxRec";
            this.btnMaxRec.Size = new System.Drawing.Size(108, 28);
            this.btnMaxRec.TabIndex = 8;
            this.btnMaxRec.Text = "Mas Recaudador";
            this.btnMaxRec.UseVisualStyleBackColor = true;
            this.btnMaxRec.Click += new System.EventHandler(this.btnMaxRec_Click);
            // 
            // lblRecaudacionTotal
            // 
            this.lblRecaudacionTotal.AutoSize = true;
            this.lblRecaudacionTotal.Location = new System.Drawing.Point(145, 37);
            this.lblRecaudacionTotal.Name = "lblRecaudacionTotal";
            this.lblRecaudacionTotal.Size = new System.Drawing.Size(10, 13);
            this.lblRecaudacionTotal.TabIndex = 7;
            this.lblRecaudacionTotal.Text = "-";
            // 
            // total
            // 
            this.total.AutoSize = true;
            this.total.Location = new System.Drawing.Point(15, 37);
            this.total.Name = "total";
            this.total.Size = new System.Drawing.Size(98, 13);
            this.total.TabIndex = 6;
            this.total.Text = "Recaudacion Total";
            // 
            // resusurt3
            // 
            this.resusurt3.AutoSize = true;
            this.resusurt3.Location = new System.Drawing.Point(145, 123);
            this.resusurt3.Name = "resusurt3";
            this.resusurt3.Size = new System.Drawing.Size(10, 13);
            this.resusurt3.TabIndex = 5;
            this.resusurt3.Text = "-";
            // 
            // resusurt2
            // 
            this.resusurt2.AutoSize = true;
            this.resusurt2.Location = new System.Drawing.Point(145, 100);
            this.resusurt2.Name = "resusurt2";
            this.resusurt2.Size = new System.Drawing.Size(10, 13);
            this.resusurt2.TabIndex = 4;
            this.resusurt2.Text = "-";
            // 
            // resusurt1
            // 
            this.resusurt1.AutoSize = true;
            this.resusurt1.Location = new System.Drawing.Point(145, 72);
            this.resusurt1.Name = "resusurt1";
            this.resusurt1.Size = new System.Drawing.Size(10, 13);
            this.resusurt1.TabIndex = 3;
            this.resusurt1.Text = "-";
            // 
            // lblsurt3
            // 
            this.lblsurt3.AutoSize = true;
            this.lblsurt3.Location = new System.Drawing.Point(15, 123);
            this.lblsurt3.Name = "lblsurt3";
            this.lblsurt3.Size = new System.Drawing.Size(86, 13);
            this.lblsurt3.TabIndex = 2;
            this.lblsurt3.Text = "Surtidor Premium";
            // 
            // lblsurt2
            // 
            this.lblsurt2.AutoSize = true;
            this.lblsurt2.Location = new System.Drawing.Point(15, 100);
            this.lblsurt2.Name = "lblsurt2";
            this.lblsurt2.Size = new System.Drawing.Size(74, 13);
            this.lblsurt2.TabIndex = 1;
            this.lblsurt2.Text = "Surtidor Super";
            // 
            // lblsurt1
            // 
            this.lblsurt1.AutoSize = true;
            this.lblsurt1.Location = new System.Drawing.Point(15, 72);
            this.lblsurt1.Name = "lblsurt1";
            this.lblsurt1.Size = new System.Drawing.Size(79, 13);
            this.lblsurt1.TabIndex = 0;
            this.lblsurt1.Text = "Surtidor Normal";
            // 
            // btnRecarga
            // 
            this.btnRecarga.Location = new System.Drawing.Point(30, 261);
            this.btnRecarga.Name = "btnRecarga";
            this.btnRecarga.Size = new System.Drawing.Size(97, 44);
            this.btnRecarga.TabIndex = 15;
            this.btnRecarga.Text = "RECARGAR";
            this.btnRecarga.UseVisualStyleBackColor = true;
            this.btnRecarga.Click += new System.EventHandler(this.btnRecarga_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(701, 478);
            this.Controls.Add(this.btnRecarga);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblPrecioTotal);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnComprar);
            this.Controls.Add(this.nudLitros);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblCapacidadSurtidor);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblPrecioNafta);
            this.Controls.Add(this.sadsad);
            this.Controls.Add(this.lblTipoNafta);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbSurtidores);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudLitros)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbSurtidores;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblTipoNafta;
        private System.Windows.Forms.Label sadsad;
        private System.Windows.Forms.Label lblPrecioNafta;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblCapacidadSurtidor;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown nudLitros;
        private System.Windows.Forms.Button btnComprar;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblPrecioTotal;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label resusurt3;
        private System.Windows.Forms.Label resusurt2;
        private System.Windows.Forms.Label resusurt1;
        private System.Windows.Forms.Label lblsurt3;
        private System.Windows.Forms.Label lblsurt2;
        private System.Windows.Forms.Label lblsurt1;
        private System.Windows.Forms.Label lblRecaudacionTotal;
        private System.Windows.Forms.Label total;
        private System.Windows.Forms.Button btnRecarga;
        private System.Windows.Forms.Button btnMinRec;
        private System.Windows.Forms.Button btnMaxRec;
        private System.Windows.Forms.Button btnMaxClient;
    }
}

