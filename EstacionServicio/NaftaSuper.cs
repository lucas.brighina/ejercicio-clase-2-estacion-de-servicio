﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstacionServicio
{
    public class NaftaSuper : NaftaAbstract
    {
        public NaftaSuper(string nombre, float precio)
        {
            this.PrecioLitro = precio;
            this.Nombre = nombre;
        }
    }
}
