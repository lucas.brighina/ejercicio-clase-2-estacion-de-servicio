﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstacionServicio
{
    public class Surtidor
    {
        private string nombreSurtidor;
        private NaftaAbstract tipoNafta;
        private int capacidad;
        private float totalRecaudado;
        private int cantidadClientes;
        private int litrosVendidos;
        private int cantidadRecargas;



        public string NombreSurtidor { get => nombreSurtidor; set => nombreSurtidor = value; }
        public NaftaAbstract TipoNafta { get => tipoNafta; set => tipoNafta = value; }
        public int Capacidad { get => capacidad; set => capacidad = value; }
        public float TotalRecaudado { get => totalRecaudado; set => totalRecaudado = value; }
        public int CantidadClientes { get => cantidadClientes; set => cantidadClientes = value; }
        public int LitrosVendidos { get => litrosVendidos; set => litrosVendidos = value; }
        public int CantidadRecargas { get => cantidadRecargas; set => cantidadRecargas = value; }

        public Surtidor(string nombre, int capacidad)
        {
            this.NombreSurtidor = nombre;
            this.Capacidad = capacidad;
            this.totalRecaudado = 0;
            this.cantidadClientes = 0;
            this.litrosVendidos = 0;
            this.cantidadRecargas = 0;
            this.TipoNafta = tipoNafta;
        }

        public void RestarCapacidad(int CapacidadARestar)
        {
            this.Capacidad -= CapacidadARestar;
        }
    }
}
